package br.com.santos.atylla.service;

import br.com.santos.atylla.domain.Avaliacao;
import br.com.santos.atylla.repository.AvaliacaoRepository;
import br.com.santos.atylla.web.rest.errors.BadRequestAlertException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@Service
@Transactional
public class AvaliacaoService {


    private final AvaliacaoRepository avaliacaoRepository;

    public AvaliacaoService(AvaliacaoRepository avaliacaoRepository) {
        this.avaliacaoRepository = avaliacaoRepository;
    }

    private Avaliacao save(Avaliacao avaliacao) {
        return avaliacaoRepository.save(avaliacao);
    }

    @Transactional(readOnly = true)
    public List<Avaliacao> findAll() {
        return avaliacaoRepository.findAll();
    }

    @Transactional(readOnly = true)
    public Optional<Avaliacao> findOne(Long id) {
        return avaliacaoRepository.findById(id);
    }

    public void delete(Long id) {

        avaliacaoRepository.deleteById(id);
    }

    public Avaliacao updateAvaliacao(Avaliacao avaliacao) {
        if (avaliacao.getId() == null) {
            throw new BadRequestAlertException("ID inválido");
        }
        return save(avaliacao);
    }

    public Avaliacao createAvaliacao(Avaliacao avaliacao) {
        if (avaliacao.getId() != null) {
            throw new BadRequestAlertException("Uma nova avaliação não pode ter ID");
        }
        return save(avaliacao);
    }

    @Transactional(readOnly = true)
    public List<Avaliacao> findAllByObraId(Long obraId) {
        return avaliacaoRepository.findAllByObraId(obraId);
    }
}
