package br.com.santos.atylla.service;

import br.com.santos.atylla.domain.Autor;
import br.com.santos.atylla.domain.enumeration.Pais;
import br.com.santos.atylla.repository.AutorRepository;
import br.com.santos.atylla.web.rest.errors.BadRequestAlertException;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@Service
@Transactional
public class AutorService {

    private final ObraService obraService;

    private final AutorRepository autorRepository;

    public AutorService(AutorRepository autorRepository, @Lazy ObraService obraService) {
        this.autorRepository = autorRepository;
        this.obraService = obraService;
    }

    private Autor save(Autor autor) {
        return autorRepository.save(autor);
    }

    @Transactional(readOnly = true)
    public List<Autor> findAll() {
        return autorRepository.findAllWithEagerRelationships();
    }

    @Transactional(readOnly = true)
    public Optional<Autor> findOne(Long id) {
        return autorRepository.findOneWithEagerRelationships(id);
    }

    @Transactional(readOnly = true)
    public Optional<Autor> findOneWithOutEagerRelationships(Long id) {
        return autorRepository.findById(id);
    }

    @Transactional(readOnly = true)
    public Optional<Autor> findOneByEmail(String email) {
        return autorRepository.findOneByEmail(email);
    }

    @Transactional(readOnly = true)
    public Optional<Autor> findOneByCpf(String cpf) {
        return autorRepository.findOneByCpf(cpf);
    }

    public void delete(Long id) {
        Autor autor = new Autor();
        autor.setId(id);
        if(!obraService.findByAutors(autor).isEmpty()){
            throw new BadRequestAlertException("Autor não pode está associado a obra(s)");
        }
        autorRepository.deleteById(id);
    }

    public Autor updateAutor(Autor autor) {
        if (autor.getId() == null) {
            throw new BadRequestAlertException("ID inválido");
        }
        validarAutor(autor);
        return save(autor);
    }

    public Autor createAutor(Autor autor) {
        if (autor.getId() != null) {
            throw new BadRequestAlertException("Um novo autor não pode ter ID");
        }
        validarAutor(autor);
        return save(autor);
    }

    private void validarAutor(Autor autor){
        Optional<Autor> autorOptional = findOneByEmail(autor.getEmail());
        if(autorOptional.isPresent() && !autorOptional.get().getId().equals(autor.getId())){
            throw new BadRequestAlertException("Já existe um autor com o email informado");
        }
        if(autor.getPaisDeOrigem().equals(Pais.BRASIL) && autor.getCpf() == null){
            throw new BadRequestAlertException("Já autor com país de origem BRASIL tem que conter CPF");
        }
        autorOptional = findOneByCpf(autor.getCpf());
        if(autorOptional.isPresent() && !autorOptional.get().equals(autor)){
            throw new BadRequestAlertException("Já existe uma autor com esse CPF");
        }
    }
}
