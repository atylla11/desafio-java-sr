package br.com.santos.atylla.service.exception;

public class InvalidPasswordException extends RuntimeException {

    public InvalidPasswordException() {
        super("Senha incorreta");
    }

}
