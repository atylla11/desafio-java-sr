package br.com.santos.atylla.service;

import br.com.santos.atylla.domain.Autor;
import br.com.santos.atylla.domain.Obra;
import br.com.santos.atylla.repository.ObraRepository;
import br.com.santos.atylla.web.rest.errors.BadRequestAlertException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;

@Service
@Transactional
public class ObraService {

    private final ObraRepository obraRepository;
    private final AmazonService amazonService;
    private final AutorService autorService;

    public ObraService(ObraRepository obraRepository, AmazonService amazonService, AutorService autorService) {
        this.obraRepository = obraRepository;
        this.amazonService = amazonService;
        this.autorService = autorService;
    }

    private Obra save(Obra obra) {
        Obra result = obraRepository.save(obra);
        result.setAutors(null);
        return result;
    }

    @Transactional(readOnly = true)
    public Page<Obra> findAll(String nome, String descricao, Pageable pageable) {
        return obraRepository.findByNomeIsContainingAndDescricaoIsContaining(normalizarCampo(nome), normalizarCampo(descricao), pageable);
    }

    @Transactional(readOnly = true)
    public Optional<Obra> findOne(Long id) {
        return obraRepository.findOneWithEagerRelationships(id);
    }

    public void delete(Long id) {
        Obra obra = obraRepository.findOneWithEagerRelationships(id).get();
        for(Autor autor : obra.getAutors()){
            autor.getObras().remove(obra);
            autorService.updateAutor(autor);
        }
        obraRepository.deleteById(id);
    }

    public Obra updateObra(Obra obra) {
        if (obra.getId() == null) {
            throw new BadRequestAlertException("ID inválido");
        }
        validarObra(obra);
        vincularImagem(obra);
        relacionarComAutor(obra);
        return save(obra);
    }

    public Obra createObra(Obra obra) {
        if (obra.getId() != null) {
            throw new BadRequestAlertException("Uma nova obra não pode ter ID");
        }
        validarObra(obra);
        vincularImagem(obra);
        relacionarComAutor(obra);
        return save(obra);
    }

    private void validarObra(Obra obra){
        if (obra.getDataExposicao() == null && obra.getDataPublicacao() == null) {
            throw new BadRequestAlertException("É necessário que data exposicao e/ou data publicacao esteja preenchido.");
        }
    }

    private void vincularImagem(Obra obra){
        if(obra.getImagem() != null){
            obra.setImagemUrl(amazonService.uploadFile(obra.getImagem(), obra.getImagemContentType()));
        }
        obra.setImagem(null);
        obra.setImagemContentType(null);
    }

    private void relacionarComAutor(Obra obra){
        Set<Autor> autors = new HashSet<>();
        for(Autor autor : obra.getAutors()){
            autors.add(autorService.findOneWithOutEagerRelationships(autor.getId()).get().addObra(obra));
        }
        obra.setAutors(autors);
    }

    @Transactional(readOnly = true)
    public List<Obra> findByAutors(Autor autor){
        return obraRepository.findByAutors(autor);
    }

    private String normalizarCampo(String campo){
        if(campo == null || campo.length() == 0 || campo.equals("null")){
            return "";
        }
        return campo;
    }
}
