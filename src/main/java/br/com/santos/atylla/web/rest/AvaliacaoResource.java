package br.com.santos.atylla.web.rest;

import br.com.santos.atylla.domain.Avaliacao;
import br.com.santos.atylla.service.AvaliacaoService;
import br.com.santos.atylla.web.rest.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.Authorization;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

@Api(value = "/avaliacoes", tags = "Recurso para tratar de Avaliações")
@RestController
@RequestMapping("/api")
public class AvaliacaoResource {

    private static final String ENTIDADE_NOME = "avaliacao";

    @Value("${application.clientApp.name}")
    private String applicationName;

    private final AvaliacaoService avaliacaoService;

    public AvaliacaoResource(AvaliacaoService avaliacaoService) {
        this.avaliacaoService = avaliacaoService;
    }

    @ApiOperation(value = "Criar uma avalição", nickname = "createAvalicao", response = Avaliacao.class,
        responseContainer = "Object", authorizations = { @Authorization(value = "BearerAuth")})
    @PostMapping("/avaliacoes")
    public ResponseEntity<Avaliacao> createAvaliacao(@Valid @RequestBody Avaliacao avaliacao) throws URISyntaxException {
        Avaliacao result = avaliacaoService.createAvaliacao(avaliacao);
        return ResponseEntity.created(new URI("/api/avaliacoes/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, false, ENTIDADE_NOME, result.getId().toString()))
            .body(result);
    }

    @ApiOperation(value = "Atualizar uma avaliação", nickname = "updateAvaliacao", response = Avaliacao.class,
        responseContainer = "Object", authorizations = { @Authorization(value = "BearerAuth")})
    @PutMapping("/avaliacoes")
    public ResponseEntity<Avaliacao> updateAvaliacao(@Valid @RequestBody Avaliacao avaliacao) {
        Avaliacao result = avaliacaoService.updateAvaliacao(avaliacao);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTIDADE_NOME, avaliacao.getId().toString()))
            .body(result);
    }

    @ApiOperation(value = "Listar todos as avaliações", nickname = "getAllAvaliacaos", response = Avaliacao.class,
        responseContainer = "List", authorizations = { @Authorization(value = "BearerAuth")})
    @GetMapping("/avaliacoes")
    public List<Avaliacao> getAllAvaliacaos(@ApiParam(value = "ID da obra vinculada a avaliação") @NotNull @RequestParam("obraId") Long obraId) {
        return avaliacaoService.findAllByObraId(obraId);
    }

    @ApiOperation(value = "Selecionar uma Avaliação por ID", nickname = "getAvaliacao", response = Avaliacao.class,
        responseContainer = "Object", authorizations = { @Authorization(value = "BearerAuth")})
    @GetMapping("/avaliacoes/{id}")
    public ResponseEntity<Avaliacao> getAvaliacao(@ApiParam(value = "ID da avaliação") @PathVariable Long id) {
        Optional<Avaliacao> avaliacao = avaliacaoService.findOne(id);
        return ResponseUtil.wrapOrNotFound(avaliacao);
    }

    @ApiOperation(value = "Deletar um autor por ID", nickname = "deleteAvaliacao", response = ResponseEntity.class,
        responseContainer = "Object", authorizations = { @Authorization(value = "BearerAuth")})
    @DeleteMapping("/avaliacoes/{id}")
    public ResponseEntity<Void> deleteAvaliacao(@ApiParam(value = "ID da avaliação") @PathVariable Long id) {
        avaliacaoService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, false, ENTIDADE_NOME, id.toString())).build();
    }
}
