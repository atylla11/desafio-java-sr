package br.com.santos.atylla.web.rest;

import br.com.santos.atylla.domain.Avaliacao;
import br.com.santos.atylla.domain.Obra;
import br.com.santos.atylla.service.ObraService;
import br.com.santos.atylla.web.rest.util.HeaderUtil;
import io.github.jhipster.web.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.Authorization;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

@Api(value = "/obras", tags = "Recurso para tratar de Obras")
@RestController
@RequestMapping("/api")
public class ObraResource {

    private static final String ENTIDADE_NOME = "obra";

    @Value("${application.clientApp.name}")
    private String applicationName;

    private final ObraService obraService;

    public ObraResource(ObraService obraService) {
        this.obraService = obraService;
    }

    @ApiOperation(value = "Criar uma obra", nickname = "createObra", response = Obra.class,
        responseContainer = "Object", authorizations = { @Authorization(value = "BearerAuth")})
    @PostMapping("/obras")
    public ResponseEntity<Obra> createObra(@Valid @RequestBody Obra obra) throws URISyntaxException {
        Obra result = obraService.createObra(obra);
        return ResponseEntity.created(new URI("/api/obras/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, false, ENTIDADE_NOME, result.getId().toString()))
            .body(result);
    }

    @ApiOperation(value = "Atualizar uma obra", nickname = "updateObra", response = Obra.class,
        responseContainer = "Object", authorizations = { @Authorization(value = "BearerAuth")})
    @PutMapping("/obras")
    public ResponseEntity<Obra> updateObra(@Valid @RequestBody Obra obra) {
        Obra result = obraService.updateObra(obra);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTIDADE_NOME, obra.getId().toString()))
            .body(result);
    }

    @ApiOperation(value = "Listar todos as obras", nickname = "getAllObras", response = Obra.class,
        responseContainer = "List", authorizations = { @Authorization(value = "BearerAuth")})
    @GetMapping("/obras")
    public ResponseEntity<List<Obra>> getAllObras(@ApiParam(value = "Nome da obra que deseja buscar") @RequestParam(required = false) String nome, @ApiParam(value = "Descrição da obra que deseja buscar") @RequestParam(required = false) String descricao, Pageable pageable) {
        Page<Obra> page = obraService.findAll(nome, descricao, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    @ApiOperation(value = "Selecionar uma obra por ID", nickname = "getObra", response = Obra.class,
        responseContainer = "Object", authorizations = { @Authorization(value = "BearerAuth")})
    @GetMapping("/obras/{id}")
    public ResponseEntity<Obra> getObra(@ApiParam(value = "ID da obra") @PathVariable Long id) {
        Optional<Obra> obra = obraService.findOne(id);
        return ResponseUtil.wrapOrNotFound(obra);
    }

    @ApiOperation(value = "Deletar uma obra por ID", nickname = "deleteObra", response = ResponseEntity.class,
        responseContainer = "Object", authorizations = { @Authorization(value = "BearerAuth")})
    @DeleteMapping("/obras/{id}")
    public ResponseEntity<Void> deleteObra(@ApiParam(value = "ID da obra") @PathVariable Long id) {
        obraService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, false, ENTIDADE_NOME, id.toString())).build();
    }
}
