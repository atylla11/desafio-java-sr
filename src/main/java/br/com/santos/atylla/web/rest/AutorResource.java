package br.com.santos.atylla.web.rest;

import br.com.santos.atylla.domain.Autor;
import br.com.santos.atylla.service.AutorService;
import br.com.santos.atylla.web.rest.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import io.swagger.annotations.*;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

@Api(value = "/autores", tags = "Recurso para tratar de Autor")
@RestController
@RequestMapping("/api")
public class AutorResource {

    private static final String ENTIDADE_NOME = "autor";

    @Value("${application.clientApp.name}")
    private String applicationName;

    private final AutorService autorService;

    public AutorResource(AutorService autorService) {
        this.autorService = autorService;
    }

    @ApiOperation(value = "Criar um autor", nickname = "createAutor", response = Autor.class,
        responseContainer = "Object", authorizations = { @Authorization(value = "BearerAuth")})
    @PostMapping("/autores")
    public ResponseEntity<Autor> createAutor(@Valid @RequestBody Autor autor) throws URISyntaxException {
        Autor result = autorService.createAutor(autor);
        return ResponseEntity.created(new URI("/api/autores/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, false, ENTIDADE_NOME, result.getId().toString()))
            .body(result);
    }

    @ApiOperation(value = "Atualizar um autor", nickname = "updateAutor", response = Autor.class,
        responseContainer = "Object", authorizations = { @Authorization(value = "BearerAuth")})
    @PutMapping("/autores")
    public ResponseEntity<Autor> updateAutor(@Valid @RequestBody Autor autor) throws URISyntaxException {
        Autor result = autorService.updateAutor(autor);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTIDADE_NOME, autor.getId().toString()))
            .body(result);
    }

    @ApiOperation(value = "Listar todos os autores", nickname = "getAllAutors", response = Autor.class,
        responseContainer = "List", authorizations = { @Authorization(value = "BearerAuth")})
    @GetMapping("/autores")
    public List<Autor> getAllAutors() {
        return autorService.findAll();
    }

    @ApiOperation(value = "Selecionar um autor por ID", nickname = "getAutor", response = Autor.class,
        responseContainer = "Object", authorizations = { @Authorization(value = "BearerAuth")})
    @GetMapping("/autores/{id}")
    public ResponseEntity<Autor> getAutor(@ApiParam(value = "ID do autor") @PathVariable Long id) {
        Optional<Autor> autor = autorService.findOne(id);
        return ResponseUtil.wrapOrNotFound(autor);
    }

    @ApiOperation(value = "Deletar um autor por ID", nickname = "deleteAutor", response = ResponseEntity.class,
        responseContainer = "Object", authorizations = { @Authorization(value = "BearerAuth")})
    @DeleteMapping("/autores/{id}")
    public ResponseEntity<Void> deleteAutor(@ApiParam(value = "ID do autor") @PathVariable Long id) {
        autorService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, false, ENTIDADE_NOME, id.toString())).build();
    }

}
