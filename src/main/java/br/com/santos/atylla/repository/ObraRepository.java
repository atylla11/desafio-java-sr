package br.com.santos.atylla.repository;

import br.com.santos.atylla.domain.Autor;
import br.com.santos.atylla.domain.Obra;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;


@Repository
public interface ObraRepository extends JpaRepository<Obra, Long> {

    @Query("select obra from Obra obra left join fetch obra.autors where obra.id =:id")
    Optional<Obra> findOneWithEagerRelationships(@Param("id") Long id);
    List<Obra> findByAutors(Autor autor);
    Page<Obra> findByNomeIsContainingAndDescricaoIsContaining(String nome, String descricao, Pageable pageable);
}
