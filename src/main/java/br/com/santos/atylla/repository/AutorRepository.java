package br.com.santos.atylla.repository;

import br.com.santos.atylla.domain.Autor;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface AutorRepository extends JpaRepository<Autor, Long> {

    @Query("select distinct autor from Autor autor left join fetch autor.obras")
    List<Autor> findAllWithEagerRelationships();

    @Query("select autor from Autor autor left join fetch autor.obras where autor.id =:id")
    Optional<Autor> findOneWithEagerRelationships(@Param("id") Long id);

    Optional<Autor> findOneByEmail(@Param("email") String email);

    Optional<Autor> findOneByCpf(@Param("cpf") String cpf);


}
