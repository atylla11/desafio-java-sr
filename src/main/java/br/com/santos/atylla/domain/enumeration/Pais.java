package br.com.santos.atylla.domain.enumeration;

/**
 * The Pais enumeration.
 */
public enum Pais {
    BRASIL, EUA, ARGENTINA, CHILE
}
