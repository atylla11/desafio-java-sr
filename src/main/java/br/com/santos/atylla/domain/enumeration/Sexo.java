package br.com.santos.atylla.domain.enumeration;

/**
 * The Sexo enumeration.
 */
public enum Sexo {
    MASCULINO, FEMININO
}
