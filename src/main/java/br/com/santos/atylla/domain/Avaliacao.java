package br.com.santos.atylla.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import javax.persistence.*;
import javax.validation.constraints.*;
import java.io.Serializable;
import java.time.LocalDate;

@Entity
@Table(name = "avaliacao")
public class Avaliacao implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotNull
    @Column(name = "nome_avaliador", nullable = false)
    private String nomeAvaliador;

    @Size(max = 500)
    @Column(name = "descricao", length = 500)
    private String descricao;

    @NotNull
    @Min(value = 1)
    @Max(value = 5)
    @Column(name = "nota", nullable = false)
    private Integer nota;

    @NotNull
    @Pattern(regexp = "^[^@\\s]+@[^@\\s]+\\.[^@\\s]+$")
    @Column(name = "email_avaliador", nullable = false)
    private String emailAvaliador;

    @NotNull
    @Column(name = "data_avaliacao", nullable = false)
    private LocalDate dataAvaliacao;

    @ManyToOne
    @JsonIgnoreProperties("avaliacaos")
    private Obra obra;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNomeAvaliador() {
        return nomeAvaliador;
    }

    public Avaliacao nomeAvaliador(String nomeAvaliador) {
        this.nomeAvaliador = nomeAvaliador;
        return this;
    }

    public void setNomeAvaliador(String nomeAvaliador) {
        this.nomeAvaliador = nomeAvaliador;
    }

    public String getDescricao() {
        return descricao;
    }

    public Avaliacao descricao(String descricao) {
        this.descricao = descricao;
        return this;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public Integer getNota() {
        return nota;
    }

    public Avaliacao nota(Integer nota) {
        this.nota = nota;
        return this;
    }

    public void setNota(Integer nota) {
        this.nota = nota;
    }

    public String getEmailAvaliador() {
        return emailAvaliador;
    }

    public Avaliacao emailAvaliador(String emailAvaliador) {
        this.emailAvaliador = emailAvaliador;
        return this;
    }

    public void setEmailAvaliador(String emailAvaliador) {
        this.emailAvaliador = emailAvaliador;
    }

    public LocalDate getDataAvaliacao() {
        return dataAvaliacao;
    }

    public Avaliacao dataAvaliacao(LocalDate dataAvaliacao) {
        this.dataAvaliacao = dataAvaliacao;
        return this;
    }

    public void setDataAvaliacao(LocalDate dataAvaliacao) {
        this.dataAvaliacao = dataAvaliacao;
    }

    public Obra getObra() {
        return obra;
    }

    public Avaliacao obra(Obra obra) {
        this.obra = obra;
        return this;
    }

    public void setObra(Obra obra) {
        this.obra = obra;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Avaliacao)) {
            return false;
        }
        return id != null && id.equals(((Avaliacao) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    @Override
    public String toString() {
        return "Avaliacao{" +
            "id=" + getId() +
            ", nomeAvaliador='" + getNomeAvaliador() + "'" +
            ", descricao='" + getDescricao() + "'" +
            ", nota=" + getNota() +
            ", emailAvaliador='" + getEmailAvaliador() + "'" +
            ", dataAvaliacao='" + getDataAvaliacao() + "'" +
            "}";
    }
}
