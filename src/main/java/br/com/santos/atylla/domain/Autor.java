package br.com.santos.atylla.domain;

import br.com.santos.atylla.domain.enumeration.Pais;
import br.com.santos.atylla.domain.enumeration.Sexo;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name = "autor")
public class Autor implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotNull
    @Column(name = "nome", nullable = false)
    private String nome;

    @NotNull
    @Enumerated(EnumType.STRING)
    @Column(name = "sexo", nullable = false)
    private Sexo sexo;

    @NotNull
    @Pattern(regexp = "^[^@\\s]+@[^@\\s]+\\.[^@\\s]+$")
    @Column(name = "email")
    private String email;

    @NotNull
    @Enumerated(EnumType.STRING)
    @Column(name = "pais_de_origem", nullable = false)
    private Pais paisDeOrigem;

    @Column(name = "cpf")
    private String cpf;

    @ManyToMany
    @JoinTable(name = "autor_obra",
               joinColumns = @JoinColumn(name = "autor_id", referencedColumnName = "id"),
               inverseJoinColumns = @JoinColumn(name = "obra_id", referencedColumnName = "id"))
    private Set<Obra> obras = new HashSet<>();

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public Autor nome(String nome) {
        this.nome = nome;
        return this;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public Sexo getSexo() {
        return sexo;
    }

    public Autor sexo(Sexo sexo) {
        this.sexo = sexo;
        return this;
    }

    public void setSexo(Sexo sexo) {
        this.sexo = sexo;
    }

    public String getEmail() {
        return email;
    }

    public Autor email(String email) {
        this.email = email;
        return this;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Pais getPaisDeOrigem() {
        return paisDeOrigem;
    }

    public Autor paisDeOrigem(Pais paisDeOrigem) {
        this.paisDeOrigem = paisDeOrigem;
        return this;
    }

    public void setPaisDeOrigem(Pais paisDeOrigem) {
        this.paisDeOrigem = paisDeOrigem;
    }

    public String getCpf() {
        return cpf;
    }

    public Autor cpf(String cpf) {
        this.cpf = cpf;
        return this;
    }

    public void setCpf(String cpf) {
        this.cpf = cpf;
    }

    public Set<Obra> getObras() {
        return obras;
    }

    public Autor obras(Set<Obra> obras) {
        this.obras = obras;
        return this;
    }

    public Autor addObra(Obra obra) {
        this.obras.add(obra);
        obra.getAutors().add(this);
        return this;
    }

    public Autor removeObra(Obra obra) {
        this.obras.remove(obra);
        obra.getAutors().remove(this);
        return this;
    }

    public void setObras(Set<Obra> obras) {
        this.obras = obras;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Autor)) {
            return false;
        }
        return id != null && id.equals(((Autor) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    @Override
    public String toString() {
        return "Autor{" +
            "id=" + getId() +
            ", nome='" + getNome() + "'" +
            ", sexo='" + getSexo() + "'" +
            ", email='" + getEmail() + "'" +
            ", paisDeOrigem='" + getPaisDeOrigem() + "'" +
            ", cpf='" + getCpf() + "'" +
            "}";
    }
}
