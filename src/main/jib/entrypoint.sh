#!/bin/sh

echo "The application will start in ${TIME_TO_MARIA_DB_START=120}s..." && sleep ${TIME_TO_MARIA_DB_START=120}
exec java ${JAVA_OPTS} -noverify -XX:+AlwaysPreTouch -Djava.security.egd=file:/dev/./urandom -cp /app/resources/:/app/classes/:/app/libs/* "br.com.santos.atylla.DesafioJavaSrApp"  "$@"
