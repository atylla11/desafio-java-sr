import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { JhiDataUtils, JhiEventManager } from 'ng-jhipster';

import { IObra } from 'app/shared/model/obra.model';
import { HttpResponse } from '@angular/common/http';
import { IAvaliacao } from 'app/shared/model/avaliacao.model';
import { AvaliacaoService } from 'app/entities/avaliacao/avaliacao.service';
import { AvaliacaoDeleteDialogComponent } from 'app/entities/avaliacao/avaliacao-delete-dialog.component';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { Subscription } from 'rxjs';

@Component({
  selector: 'jhi-obra-detail',
  templateUrl: './obra-detail.component.html'
})
export class ObraDetailComponent implements OnInit {
  obra: IObra | null = null;
  avaliacaos?: IAvaliacao[];
  eventSubscriber?: Subscription;

  constructor(
    protected dataUtils: JhiDataUtils,
    protected activatedRoute: ActivatedRoute,
    protected avaliacaoService: AvaliacaoService,
    protected modalService: NgbModal,
    protected eventManager: JhiEventManager
  ) {}

  loadAll(): void {
    if (this.obra) {
      this.avaliacaoService.query({ obraId: this.obra.id }).subscribe((res: HttpResponse<IAvaliacao[]>) => {
        this.avaliacaos = res.body ? res.body : [];
      });
    }
  }

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ obra }) => {
      this.obra = obra;
    });
    this.loadAll();
    this.registerChangeInAvaliacoes();
  }

  byteSize(base64String: string): string {
    return this.dataUtils.byteSize(base64String);
  }

  openFile(contentType: string, base64String: string): void {
    this.dataUtils.openFile(contentType, base64String);
  }

  goToLink(url: string) {
    window.open(url, '_blank');
  }

  previousState(): void {
    window.history.back();
  }

  delete(avaliacao: IAvaliacao): void {
    const modalRef = this.modalService.open(AvaliacaoDeleteDialogComponent, { size: 'lg', backdrop: 'static' });
    modalRef.componentInstance.avaliacao = avaliacao;
  }

  registerChangeInAvaliacoes(): void {
    this.eventSubscriber = this.eventManager.subscribe('avaliacaoListModification', () => this.loadAll());
  }

  trackId(index: number, item: IAvaliacao): number {
    // eslint-disable-next-line @typescript-eslint/no-unnecessary-type-assertion
    return item.id!;
  }
}
