import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, Routes, Router } from '@angular/router';
import { JhiResolvePagingParams } from 'ng-jhipster';
import { Observable, of, EMPTY } from 'rxjs';
import { flatMap } from 'rxjs/operators';

import { UserRouteAccessService } from 'app/core/auth/user-route-access-service';
import { IObra, Obra } from 'app/shared/model/obra.model';
import { ObraService } from './obra.service';
import { ObraComponent } from './obra.component';
import { ObraDetailComponent } from './obra-detail.component';
import { ObraUpdateComponent } from './obra-update.component';
import { AvaliacaoUpdateComponent } from 'app/entities/avaliacao/avaliacao-update.component';
import { Avaliacao, IAvaliacao } from 'app/shared/model/avaliacao.model';
import { AvaliacaoDetailComponent } from 'app/entities/avaliacao/avaliacao-detail.component';
import { AvaliacaoService } from 'app/entities/avaliacao/avaliacao.service';

@Injectable({ providedIn: 'root' })
export class ObraResolve implements Resolve<IObra> {
  constructor(private service: ObraService, private router: Router) {}

  resolve(route: ActivatedRouteSnapshot): Observable<IObra> | Observable<never> {
    const id = route.params['id'];
    if (id) {
      return this.service.find(id).pipe(
        flatMap((obra: HttpResponse<Obra>) => {
          if (obra.body) {
            return of(obra.body);
          } else {
            this.router.navigate(['404']);
            return EMPTY;
          }
        })
      );
    }
    return of(new Obra());
  }
}

@Injectable({ providedIn: 'root' })
export class AvaliacaoResolve implements Resolve<IAvaliacao> {
  constructor(private service: AvaliacaoService, private router: Router) {}

  resolve(route: ActivatedRouteSnapshot): Observable<IAvaliacao> | Observable<never> {
    const id = route.params['id'];
    if (id) {
      return this.service.find(id).pipe(
        flatMap((avaliacao: HttpResponse<Avaliacao>) => {
          if (avaliacao.body) {
            return of(avaliacao.body);
          } else {
            this.router.navigate(['404']);
            return EMPTY;
          }
        })
      );
    }
    return of(new Avaliacao());
  }
}

export const obraRoute: Routes = [
  {
    path: '',
    component: ObraComponent,
    resolve: {
      pagingParams: JhiResolvePagingParams
    },
    data: {
      authorities: ['ROLE_USER'],
      defaultSort: 'id,asc',
      pageTitle: 'Obras'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/view',
    component: ObraDetailComponent,
    resolve: {
      obra: ObraResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'Obras'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':idObra/view/avaliacao/new',
    component: AvaliacaoUpdateComponent,
    resolve: {
      avaliacao: AvaliacaoResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'Avaliacaos'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':idObra/view/avaliacao/:id/edit',
    component: AvaliacaoUpdateComponent,
    resolve: {
      avaliacao: AvaliacaoResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'Avaliacaos'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':idObra/view/avaliacao/:id/view',
    component: AvaliacaoDetailComponent,
    resolve: {
      avaliacao: AvaliacaoResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'Avaliacaos'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: 'new',
    component: ObraUpdateComponent,
    resolve: {
      obra: ObraResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'Obras'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/edit',
    component: ObraUpdateComponent,
    resolve: {
      obra: ObraResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'Obras'
    },
    canActivate: [UserRouteAccessService]
  }
];
