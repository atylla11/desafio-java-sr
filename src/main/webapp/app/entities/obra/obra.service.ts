import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import * as moment from 'moment';

// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { DATE_FORMAT } from 'app/shared/constants/input.constants';
import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared/util/request-util';
import { IObra } from 'app/shared/model/obra.model';

type EntityResponseType = HttpResponse<IObra>;
type EntityArrayResponseType = HttpResponse<IObra[]>;

@Injectable({ providedIn: 'root' })
export class ObraService {
  public resourceUrl = SERVER_API_URL + 'api/obras';

  constructor(protected http: HttpClient) {}

  create(obra: IObra): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(obra);
    return this.http
      .post<IObra>(this.resourceUrl, copy, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  update(obra: IObra): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(obra);
    return this.http
      .put<IObra>(this.resourceUrl, copy, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http
      .get<IObra>(`${this.resourceUrl}/${id}`, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http
      .get<IObra[]>(this.resourceUrl, { params: options, observe: 'response' })
      .pipe(map((res: EntityArrayResponseType) => this.convertDateArrayFromServer(res)));
  }

  delete(id: number): Observable<HttpResponse<{}>> {
    return this.http.delete(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  protected convertDateFromClient(obra: IObra): IObra {
    const copy: IObra = Object.assign({}, obra, {
      dataPublicacao: obra.dataPublicacao && obra.dataPublicacao.isValid() ? obra.dataPublicacao.format(DATE_FORMAT) : undefined,
      dataExposicao: obra.dataExposicao && obra.dataExposicao.isValid() ? obra.dataExposicao.format(DATE_FORMAT) : undefined
    });
    return copy;
  }

  protected convertDateFromServer(res: EntityResponseType): EntityResponseType {
    if (res.body) {
      res.body.dataPublicacao = res.body.dataPublicacao ? moment(res.body.dataPublicacao) : undefined;
      res.body.dataExposicao = res.body.dataExposicao ? moment(res.body.dataExposicao) : undefined;
    }
    return res;
  }

  protected convertDateArrayFromServer(res: EntityArrayResponseType): EntityArrayResponseType {
    if (res.body) {
      res.body.forEach((obra: IObra) => {
        obra.dataPublicacao = obra.dataPublicacao ? moment(obra.dataPublicacao) : undefined;
        obra.dataExposicao = obra.dataExposicao ? moment(obra.dataExposicao) : undefined;
      });
    }
    return res;
  }
}
