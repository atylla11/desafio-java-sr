import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { AbstractControl, FormBuilder, FormControl, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { JhiDataUtils, JhiFileLoadError, JhiEventManager, JhiEventWithContent } from 'ng-jhipster';

import { IObra, Obra } from 'app/shared/model/obra.model';
import { ObraService } from './obra.service';
import { AlertError } from 'app/shared/alert/alert-error.model';
import { Moment } from 'moment';
import { IAutor } from 'app/shared/model/autor.model';
import { map } from 'rxjs/operators';
import { AutorService } from 'app/entities/autor/autor.service';

@Component({
  selector: 'jhi-obra-update',
  templateUrl: './obra-update.component.html'
})
export class ObraUpdateComponent implements OnInit {
  isSaving = false;
  dataPublicacaoDp: any;
  dataExposicaoDp: any;

  autors: IAutor[] = [];

  editForm = this.fb.group({
    id: [],
    nome: [null, [Validators.required]],
    descricao: [null, [Validators.required, Validators.minLength(1), Validators.maxLength(240)]],
    imagem: [null, [Validators.required]],
    imagemContentType: [],
    imagemUrl: [],
    dataPublicacao: [null, Validators.required],
    dataExposicao: [null, Validators.required],
    autors: []
  });
  dataPublicacao = this.editForm.get('dataPublicacao');
  dataExposicao = this.editForm.get('dataExposicao');

  constructor(
    protected dataUtils: JhiDataUtils,
    protected eventManager: JhiEventManager,
    protected obraService: ObraService,
    protected activatedRoute: ActivatedRoute,
    private fb: FormBuilder,
    protected autorService: AutorService
  ) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ obra }) => {
      this.updateForm(obra);
    });
    this.observarObrigatoriedadeExposicao();
    this.observarObrigatoriedadePublicacao();
    this.obrigatoridadeImagem();
    this.autorService
      .query()
      .pipe(
        map((res: HttpResponse<IAutor[]>) => {
          return res.body ? res.body : [];
        })
      )
      .subscribe((resBody: IAutor[]) => (this.autors = resBody));
    this.setarValidacaoDosCampos();
  }

  applicarValidacao(valor: Moment, campo: AbstractControl): void {
    if (valor) {
      if (campo) {
        this.removerValidacaoCampo(campo);
      }
    } else {
      if (campo) {
        campo.setValidators(Validators.required);
        campo.updateValueAndValidity({ emitEvent: false });
      }
    }
  }

  removerValidacaoCampo(campo: AbstractControl): void {
    if (campo) {
      campo.clearValidators();
      campo.updateValueAndValidity({ emitEvent: false });
    }
  }

  setarValidacaoDosCampos(): void {
    if (this.editForm.get('dataExposicao')!.value) {
      this.removerValidacaoCampo(this.editForm.get('dataPublicacao')!);
    }
    if (this.editForm.get('dataPublicacao')!.value) {
      this.removerValidacaoCampo(this.editForm.get('dataExposicao')!);
    }
  }

  observarObrigatoriedadeExposicao(): void {
    this.editForm.get('dataExposicao')!.valueChanges.subscribe(valor => {
      this.applicarValidacao(valor, this.editForm.get('dataPublicacao')!);
    });
  }

  observarObrigatoriedadePublicacao(): void {
    this.editForm.get('dataPublicacao')!.valueChanges.subscribe(valor => {
      this.applicarValidacao(valor, this.editForm.get('dataExposicao')!);
    });
  }

  obrigatoridadeImagem(): void {
    if (this.editForm.get('imagemUrl')!.value) {
      this.editForm.get('imagem')!.clearValidators();
      this.editForm.get('imagem')!.updateValueAndValidity({ emitEvent: false });
    }
  }

  updateForm(obra: IObra): void {
    this.editForm.patchValue({
      id: obra.id,
      nome: obra.nome,
      descricao: obra.descricao,
      imagem: obra.imagem,
      imagemContentType: obra.imagemContentType,
      imagemUrl: obra.imagemUrl,
      dataPublicacao: obra.dataPublicacao,
      dataExposicao: obra.dataExposicao,
      autors: obra.autors
    });
  }

  byteSize(base64String: string): string {
    return this.dataUtils.byteSize(base64String);
  }

  openFile(contentType: string, base64String: string): void {
    this.dataUtils.openFile(contentType, base64String);
  }

  setFileData(event: Event, field: string, isImage: boolean): void {
    this.dataUtils.loadFileToForm(event, this.editForm, field, isImage).subscribe(null, (err: JhiFileLoadError) => {
      this.eventManager.broadcast(
        new JhiEventWithContent<AlertError>('desafioJavaSrApp.error', { message: err.message })
      );
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const obra = this.createFromForm();
    if (obra.id !== undefined) {
      this.subscribeToSaveResponse(this.obraService.update(obra));
    } else {
      this.subscribeToSaveResponse(this.obraService.create(obra));
    }
  }

  private createFromForm(): IObra {
    return {
      ...new Obra(),
      id: this.editForm.get(['id'])!.value,
      nome: this.editForm.get(['nome'])!.value,
      descricao: this.editForm.get(['descricao'])!.value,
      imagemContentType: this.editForm.get(['imagemContentType'])!.value,
      imagem: this.editForm.get(['imagem'])!.value,
      imagemUrl: this.editForm.get(['imagemUrl'])!.value,
      dataPublicacao: this.editForm.get(['dataPublicacao'])!.value,
      dataExposicao: this.editForm.get(['dataExposicao'])!.value,
      autors: this.editForm.get(['autors'])!.value
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IObra>>): void {
    result.subscribe(
      () => this.onSaveSuccess(),
      () => this.onSaveError()
    );
  }

  protected onSaveSuccess(): void {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError(): void {
    this.isSaving = false;
  }

  getSelected(selectedVals: IAutor[], option: IAutor): IAutor {
    if (selectedVals) {
      for (let i = 0; i < selectedVals.length; i++) {
        if (option.id === selectedVals[i].id) {
          return selectedVals[i];
        }
      }
    }
    return option;
  }

  trackById(index: number, item: IAutor): any {
    return item.id;
  }
}
