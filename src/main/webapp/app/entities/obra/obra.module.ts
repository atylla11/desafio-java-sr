import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { DesafioJavaSrSharedModule } from 'app/shared/shared.module';
import { ObraComponent } from './obra.component';
import { ObraDetailComponent } from './obra-detail.component';
import { ObraUpdateComponent } from './obra-update.component';
import { ObraDeleteDialogComponent } from './obra-delete-dialog.component';
import { AvaliacaoUpdateComponent } from 'app/entities/avaliacao/avaliacao-update.component';
import { AvaliacaoDetailComponent } from 'app/entities/avaliacao/avaliacao-detail.component';
import { AvaliacaoDeleteDialogComponent } from 'app/entities/avaliacao/avaliacao-delete-dialog.component';
import { obraRoute } from './obra.route';

@NgModule({
  imports: [DesafioJavaSrSharedModule, RouterModule.forChild(obraRoute)],
  declarations: [
    ObraComponent,
    ObraDetailComponent,
    ObraUpdateComponent,
    ObraDeleteDialogComponent,
    AvaliacaoUpdateComponent,
    AvaliacaoDetailComponent,
    AvaliacaoDeleteDialogComponent
  ],
  entryComponents: [ObraDeleteDialogComponent, AvaliacaoDeleteDialogComponent]
})
export class DesafioJavaSrObraModule {}
