import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import * as moment from 'moment';

// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { DATE_FORMAT } from 'app/shared/constants/input.constants';
import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared/util/request-util';
import { IAvaliacao } from 'app/shared/model/avaliacao.model';

type EntityResponseType = HttpResponse<IAvaliacao>;
type EntityArrayResponseType = HttpResponse<IAvaliacao[]>;

@Injectable({ providedIn: 'root' })
export class AvaliacaoService {
  public resourceUrl = SERVER_API_URL + 'api/avaliacoes';

  constructor(protected http: HttpClient) {}

  create(avaliacao: IAvaliacao): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(avaliacao);
    return this.http
      .post<IAvaliacao>(this.resourceUrl, copy, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  update(avaliacao: IAvaliacao): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(avaliacao);
    return this.http
      .put<IAvaliacao>(this.resourceUrl, copy, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http
      .get<IAvaliacao>(`${this.resourceUrl}/${id}`, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http
      .get<IAvaliacao[]>(this.resourceUrl, { params: options, observe: 'response' })
      .pipe(map((res: EntityArrayResponseType) => this.convertDateArrayFromServer(res)));
  }

  delete(id: number): Observable<HttpResponse<{}>> {
    return this.http.delete(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  protected convertDateFromClient(avaliacao: IAvaliacao): IAvaliacao {
    const copy: IAvaliacao = Object.assign({}, avaliacao, {
      dataAvaliacao: avaliacao.dataAvaliacao && avaliacao.dataAvaliacao.isValid() ? avaliacao.dataAvaliacao.format(DATE_FORMAT) : undefined
    });
    return copy;
  }

  protected convertDateFromServer(res: EntityResponseType): EntityResponseType {
    if (res.body) {
      res.body.dataAvaliacao = res.body.dataAvaliacao ? moment(res.body.dataAvaliacao) : undefined;
    }
    return res;
  }

  protected convertDateArrayFromServer(res: EntityArrayResponseType): EntityArrayResponseType {
    if (res.body) {
      res.body.forEach((avaliacao: IAvaliacao) => {
        avaliacao.dataAvaliacao = avaliacao.dataAvaliacao ? moment(avaliacao.dataAvaliacao) : undefined;
      });
    }
    return res;
  }
}
