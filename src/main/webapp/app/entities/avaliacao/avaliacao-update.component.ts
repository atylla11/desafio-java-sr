import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

import { IAvaliacao, Avaliacao } from 'app/shared/model/avaliacao.model';
import { AvaliacaoService } from './avaliacao.service';
import { IObra } from 'app/shared/model/obra.model';
import { ObraService } from 'app/entities/obra/obra.service';

@Component({
  selector: 'jhi-avaliacao-update',
  templateUrl: './avaliacao-update.component.html'
})
export class AvaliacaoUpdateComponent implements OnInit {
  isSaving = false;

  idObra = 0;
  obras: IObra[] = [];
  dataAvaliacaoDp: any;

  editForm = this.fb.group({
    id: [],
    nomeAvaliador: [null, [Validators.required]],
    descricao: [null, [Validators.maxLength(500)]],
    nota: [null, [Validators.required, Validators.min(1), Validators.max(5)]],
    emailAvaliador: [null, [Validators.required, Validators.pattern('^[^@\\s]+@[^@\\s]+\\.[^@\\s]+$')]],
    dataAvaliacao: [null, [Validators.required]]
  });

  constructor(
    protected avaliacaoService: AvaliacaoService,
    protected obraService: ObraService,
    protected activatedRoute: ActivatedRoute,
    private fb: FormBuilder,
    private router: Router
  ) {}

  ngOnInit(): void {
    this.activatedRoute.params.subscribe(valor => {
      this.idObra = valor['idObra'];
    });
    this.activatedRoute.data.subscribe(({ avaliacao }) => {
      this.updateForm(avaliacao);

      this.obraService
        .query()
        .pipe(
          map((res: HttpResponse<IObra[]>) => {
            return res.body ? res.body : [];
          })
        )
        .subscribe((resBody: IObra[]) => (this.obras = resBody));
    });
  }

  updateForm(avaliacao: IAvaliacao): void {
    this.editForm.patchValue({
      id: avaliacao.id,
      nomeAvaliador: avaliacao.nomeAvaliador,
      descricao: avaliacao.descricao,
      nota: avaliacao.nota,
      emailAvaliador: avaliacao.emailAvaliador,
      dataAvaliacao: avaliacao.dataAvaliacao
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const avaliacao = this.createFromForm();
    if (avaliacao.id !== undefined) {
      this.subscribeToSaveResponse(this.avaliacaoService.update(avaliacao));
    } else {
      this.subscribeToSaveResponse(this.avaliacaoService.create(avaliacao));
    }
  }

  private createFromForm(): IAvaliacao {
    return {
      ...new Avaliacao(),
      id: this.editForm.get(['id'])!.value,
      nomeAvaliador: this.editForm.get(['nomeAvaliador'])!.value,
      descricao: this.editForm.get(['descricao'])!.value,
      nota: this.editForm.get(['nota'])!.value,
      emailAvaliador: this.editForm.get(['emailAvaliador'])!.value,
      dataAvaliacao: this.editForm.get(['dataAvaliacao'])!.value,
      obra: { id: this.idObra }
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IAvaliacao>>): void {
    result.subscribe(
      () => this.onSaveSuccess(),
      () => this.onSaveError()
    );
  }

  protected onSaveSuccess(): void {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError(): void {
    this.isSaving = false;
  }

  trackById(index: number, item: IObra): any {
    return item.id;
  }
}
