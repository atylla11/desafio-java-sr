import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { DesafioJavaSrSharedModule } from 'app/shared/shared.module';
import { AutorComponent } from './autor.component';
import { AutorDetailComponent } from './autor-detail.component';
import { AutorUpdateComponent } from './autor-update.component';
import { AutorDeleteDialogComponent } from './autor-delete-dialog.component';
import { autorRoute } from './autor.route';
import { TextMaskModule } from 'angular2-text-mask';

@NgModule({
  imports: [DesafioJavaSrSharedModule, RouterModule.forChild(autorRoute), TextMaskModule],
  declarations: [AutorComponent, AutorDetailComponent, AutorUpdateComponent, AutorDeleteDialogComponent],
  entryComponents: [AutorDeleteDialogComponent]
})
export class DesafioJavaSrAutorModule {}
