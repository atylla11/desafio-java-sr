import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

import { IAutor, Autor } from 'app/shared/model/autor.model';
import { AutorService } from './autor.service';
import { IObra } from 'app/shared/model/obra.model';
import { ObraService } from 'app/entities/obra/obra.service';

@Component({
  selector: 'jhi-autor-update',
  templateUrl: './autor-update.component.html'
})
export class AutorUpdateComponent implements OnInit {
  isSaving = false;

  obras: IObra[] = [];
  cfgMascara = { mask: [/\d/, /\d/, /\d/, '.', /\d/, /\d/, /\d/, '.', /\d/, /\d/, /\d/, '-', /\d/, /\d/] };
  editForm = this.fb.group({
    id: [],
    nome: [null, [Validators.required]],
    sexo: [null, [Validators.required]],
    email: [null, [Validators.required, Validators.pattern('^[^@\\s]+@[^@\\s]+\\.[^@\\s]+$')]],
    paisDeOrigem: [null, [Validators.required]],
    cpf: [null, [Validators.required]],
    obras: []
  });

  constructor(
    protected autorService: AutorService,
    protected obraService: ObraService,
    protected activatedRoute: ActivatedRoute,
    private fb: FormBuilder
  ) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ autor }) => {
      this.updateForm(autor);

      this.obraService
        .query({ size: 100 })
        .pipe(
          map((res: HttpResponse<IObra[]>) => {
            return res.body ? res.body : [];
          })
        )
        .subscribe((resBody: IObra[]) => (this.obras = resBody));
    });

    this.observarObrigatoriedadeCPF();
  }

  updateForm(autor: IAutor): void {
    this.editForm.patchValue({
      id: autor.id,
      nome: autor.nome,
      sexo: autor.sexo,
      email: autor.email,
      paisDeOrigem: autor.paisDeOrigem,
      cpf: autor.cpf,
      obras: autor.obras
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const autor = this.createFromForm();
    if (autor.id !== undefined) {
      this.subscribeToSaveResponse(this.autorService.update(autor));
    } else {
      this.subscribeToSaveResponse(this.autorService.create(autor));
    }
  }

  private createFromForm(): IAutor {
    return {
      ...new Autor(),
      id: this.editForm.get(['id'])!.value,
      nome: this.editForm.get(['nome'])!.value,
      sexo: this.editForm.get(['sexo'])!.value,
      email: this.editForm.get(['email'])!.value,
      paisDeOrigem: this.editForm.get(['paisDeOrigem'])!.value,
      cpf: this.editForm.get(['cpf'])!.value,
      obras: this.editForm.get(['obras'])!.value
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IAutor>>): void {
    result.subscribe(
      () => this.onSaveSuccess(),
      () => this.onSaveError()
    );
  }

  protected onSaveSuccess(): void {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError(): void {
    this.isSaving = false;
  }

  trackById(index: number, item: IObra): any {
    return item.id;
  }

  getSelected(selectedVals: IObra[], option: IObra): IObra {
    if (selectedVals) {
      for (let i = 0; i < selectedVals.length; i++) {
        if (option.id === selectedVals[i].id) {
          return selectedVals[i];
        }
      }
    }
    return option;
  }

  observarObrigatoriedadeCPF(): void {
    this.editForm.get('paisDeOrigem')!.valueChanges.subscribe(valor => {
      if (valor) {
        if (valor === 'BRASIL') {
          this.editForm.get('cpf')!.setValidators(Validators.required);
          this.editForm.get('cpf')!.updateValueAndValidity({ emitEvent: false });
        } else {
          this.editForm.get('cpf')!.setValue(null);
          this.editForm.get('cpf')!.clearValidators();
          this.editForm.get('cpf')!.updateValueAndValidity({ emitEvent: false });
        }
      }
    });
  }
}
