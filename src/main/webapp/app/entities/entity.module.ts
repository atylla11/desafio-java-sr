import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

@NgModule({
  imports: [
    RouterModule.forChild([
      {
        path: 'autor',
        loadChildren: () => import('./autor/autor.module').then(m => m.DesafioJavaSrAutorModule)
      },
      {
        path: 'obra',
        loadChildren: () => import('./obra/obra.module').then(m => m.DesafioJavaSrObraModule)
      }
    ])
  ]
})
export class DesafioJavaSrEntityModule {}
