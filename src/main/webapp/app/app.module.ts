import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import './vendor';
import { DesafioJavaSrSharedModule } from 'app/shared/shared.module';
import { DesafioJavaSrCoreModule } from 'app/core/core.module';
import { DesafioJavaSrAppRoutingModule } from './app-routing.module';
import { DesafioJavaSrHomeModule } from './home/home.module';
import { DesafioJavaSrEntityModule } from './entities/entity.module';
import { MainComponent } from './layouts/main/main.component';
import { NavbarComponent } from './layouts/navbar/navbar.component';
import { FooterComponent } from './layouts/footer/footer.component';
import { ErrorComponent } from './layouts/error/error.component';

@NgModule({
  imports: [
    BrowserModule,
    DesafioJavaSrSharedModule,
    DesafioJavaSrCoreModule,
    DesafioJavaSrHomeModule,
    DesafioJavaSrEntityModule,
    DesafioJavaSrAppRoutingModule
  ],
  declarations: [MainComponent, NavbarComponent, ErrorComponent, FooterComponent],
  bootstrap: [MainComponent]
})
export class DesafioJavaSrAppModule {}
