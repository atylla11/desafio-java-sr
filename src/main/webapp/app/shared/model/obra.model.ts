import { Moment } from 'moment';
import { IAutor } from 'app/shared/model/autor.model';

export interface IObra {
  id?: number;
  nome?: string;
  descricao?: string;
  imagemContentType?: string;
  imagem?: any;
  imagemUrl?: string;
  dataPublicacao?: Moment;
  dataExposicao?: Moment;
  autors?: IAutor[];
}

export class Obra implements IObra {
  constructor(
    public id?: number,
    public nome?: string,
    public descricao?: string,
    public imagemContentType?: string,
    public imagem?: any,
    public imagemUrl?: string,
    public dataPublicacao?: Moment,
    public dataExposicao?: Moment,
    public autors?: IAutor[]
  ) {}
}
