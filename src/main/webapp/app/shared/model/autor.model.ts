import { IObra } from 'app/shared/model/obra.model';
import { Sexo } from 'app/shared/model/enumerations/sexo.model';
import { Pais } from 'app/shared/model/enumerations/pais.model';

export interface IAutor {
  id?: number;
  nome?: string;
  sexo?: Sexo;
  email?: string;
  paisDeOrigem?: Pais;
  cpf?: string;
  obras?: IObra[];
}

export class Autor implements IAutor {
  constructor(
    public id?: number,
    public nome?: string,
    public sexo?: Sexo,
    public email?: string,
    public paisDeOrigem?: Pais,
    public cpf?: string,
    public obras?: IObra[]
  ) {}
}
