import { Moment } from 'moment';
import { IObra } from 'app/shared/model/obra.model';

export interface IAvaliacao {
  id?: number;
  nomeAvaliador?: string;
  descricao?: string;
  nota?: number;
  emailAvaliador?: string;
  dataAvaliacao?: Moment;
  obra?: IObra;
}

export class Avaliacao implements IAvaliacao {
  constructor(
    public id?: number,
    public nomeAvaliador?: string,
    public descricao?: string,
    public nota?: number,
    public emailAvaliador?: string,
    public dataAvaliacao?: Moment,
    public obra?: IObra
  ) {}
}
