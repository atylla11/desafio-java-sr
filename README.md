# Instuções para Rodar o projeto
 - Acessar a pasta /src/main/docker
 - Executar o comando: docker-compose up
 - Esperar o log de carregamento do spring
 - Acessar http://localhost:8080
 - Logar com usuário admin senha admin
 

# Seleção Dev Java
Se você chegou até aqui é porque se interessou em fazer parte do nosso quadro de funcionários. Como temos muitas oportunidades para você colocar a mão na massa, queremos ver como você se sai com o cenário abaixo, por meio do qual conseguiremos avaliar várias de suas competências.

## A demanda
Deverá ser criada uma aplicação de gerenciamento de **autores**, **obras** e **avaliações**, seguindo as regras de relacionamento abaixo:
 - Cada autor poderá ter 0 (zero) ou n obra(s);
 - Cada obra deverá ter 1 (um) ou n autor(es);
 - Cada obra poderá ter 0 (zero) ou n avaliações;
 - A partir de uma obra deverá ser posível acessar o(s) autor(es);
 - A partir de um autor deverá ser possível acessar a(s) obra(s);
 - A partir de uma obra deverá ser possível acessar as avaliações.

###  1) Back-end
A aplicação, a ser desenvolvida em Java, deverá expor uma API REST de cadastro, alteração, remoção e consulta de **autores**, **obras** e **avaliações** com as seguintes propriedades básicas definidas para cada entidade:

#### Autor
 - Nome - obrigatório
 - Sexo
 - E-mail - não obrigatório, deve ser validado caso preenchido (não pode haver dois cadastros com mesmo e-mail)
 - Data de nascimento - obrigatório, deve ser validada
 - País de origem - obrigatório (deve ser um país existente)
 - CPF - somente deve ser informado caso país de origem seja o Brasil, desta forma torna-se obrigatório. Deve ser validado (formatado e não pode haver dois cadastros com mesmo CPF)

#### Obra
 - Nome - obrigatório
 - Descrição - obrigatório (deve conter no máximo 240 caracteres)
 - Imagem - obrigatório
 - Data de publicação - obrigatória caso a data de exposição não seja informada (é utilizada mais para livros e demais publicações escritas)
 - Data de exposição - obrigatória caso a data de publicação não seja informada (é utilizada mais para obras que são expostas, como pinturas, esculturas e demais)

#### Avaliação
 - Nome do avaliador - obrigatório
 - Descrição da avaliação - opcional (deve conter no máximo 500 caracteres)
 - Nota - obrigatório (número que deve variar de 1 a 5)
 - E-mail do avaliador - obrigatório, deve ser validado
 - Data da avaliação - obrigatória

#### Regra(s)
 - A data de publicação e a data de exposição não podem ser nulas ao mesmo tempo, devendo sempre uma ou outra ser informada;
 - O **upload** da imagem da **obra** deverá ser feito via interface do seu sistema para um servidor de arquivos gratuito externo (ex.: Amazon S3, Google Drive, Dropbox, OneDrive...) utilizando a API do servidor escolhido.

### 2) Front-end
A aplicação deverá ser desenvolvida utilizando **Angular**, **React** ou outro. Deve ser acessível via navegador e possuir telas com formulários para cadastro de **autores**, **obras** e **avaliações**.

#### Autor
 - **Cadastro**: na tela de cadastro e edição dos **autores**, além de informar seus atributos básicos, deverá ser possível associar qual(is) **obra(s)** (cadastradas no sistema) são de sua autoria.
 - **Consulta**: na tela de consulta dos **autores** deverá haver uma tabela onde serão listados os autores cadastrados. Ao selecionar uma obra na tabela, deverá ser possível solicitar edição e exclusão.

#### Obra
 - **Cadastro**: na tela de cadastro e edição das **obras**, além de informar seus atributos básicos (incluindo o **upload** da imagem), deverá ser possível associar seu(s) **autor(es)** (cadastrados no sistema).
 - **Consulta**: na tela de consulta das **obras** deverá haver uma tabela onde serão listadas 15 (quinze) obras por página da tabela. Ao selecionar uma obra na tabela, deverá ser possível solicitar edição e exclusão.

#### Avaliação
 - O gerenciamento (cadastro, consulta, edição e exclusão) das avaliações deve ser feito na mesma tela de edição das obras. Ao selecionar uma avaliação na tabela, deverá ser possível solicitar edição e exclusão.

#### Regras de exclusão
 - **Autor**: somente pode ser excluído caso não possua **obras** associadas.
 - **Obra**: não há restrição para exclusão. Ao excluir uma **obra** todos os comentários referentes a ela devem também serem excluídos.
 - **Avaliação**: não há restrição para exclusão da **avaliação**.

A sua aplicação front-end deverá consumir sua API back-end. Não há restrição em relação à tecnologia para o desenvolvimento do front-end.
### 3) Filtragem e paginação
Implementar paginação e filtragem de **obra(s)**. Deve ser possível realizar a filtragem das **obra(s)** pelos atribiutos ***nome*** e ***descrição***. Deve também ser criado no front-end da consulta das **obra(s)** o *input* para realizar a filtragem.

### 4) Testes
Implementar testes de unidade utilizando JUnit. Os testes devem contemplar as operações abaixo validando as restrições informadas anteriormente:
  * **Inserção**: validar regras informadas anteriormente (ex.: obrigatoriedade dos campos em cada situação).
  * **Edição**: manter a consistência garantida anteriormente na inserção, impedindo que a entidade seja modificada para um estado inválido (ex.: **obra** sem autor, **obra** sem imagem).
  * **Consulta**: validar os dados retornados do service (pode-se utilizar mock's).
  * **Exclusão**: validar regras de exclusão informadas anteriormente.

### 5) Segurança
O acesso à aplicação só poderá ser realizado por um usuário pré-existente via autenticação **basic ou outro**.

### 6) Docker
A aplicação deverá estar disponível no repositório com os arquivos docker necessários ou em uma imagem docker (a partir do **docker-hub**) e não deve exigir configurações/parâmetros.

## Extras
Se possui conhecimento mais avançado e tempo, pode implementar as tarefas abaixo que serão consideradas como diferencial na sua análise. Atente-se para o prazo de entrega ;)
- **Extra 1 (padrão de projeto):** implementar algum padrão de projeto que auxilie o desenvolvimento, de forma a melhorar a manutenabilidade do código, diminuir a replicação e aumentar o reuso.
 - **Extra 2 (deploy/hospedagem):** a aplicação rodando em algum ambiente em nuvem;
 - **Extra 3 (teste de integração):** teste de integração da API em linguagem de sua preferência;
 - **Extra 4 (maturidade da API):** a API desenvolvida em REST, seguindo o modelo de maturidade de Richardson ou utilizando GraphQL;
 - **Extra 5 (documentação da API):** documentação executável que poderá ser utilizada durante seu desenvolvimento (utilizar Swagger);
 - **Extra 6 (auditoria):** rastrear e persistir a manipulação realizada em cada entidade (autor, obra e avaliação). Os metadados a serem persistidos devem ser: data de cadastro, data de edição e data de exclusão. Em caso de exclusão da entidade, antes de efetivada, todas as informações de suas propriedades e valores deverão ser persistidas na auditoria - seguindo a lógica "propriedade: valor".

### Instruções
- Faça o fork do desafio;
- Crie um repositório privado no Gitlab para o projeto e adicione como colaborador o usuário **selecao.stefanini.cpg**;
- Desenvolva. Você terá 4 (quatro) dias a partir da data do envio do desafio; 
- Após concluir seu trabalho faça um push para o repositório que você criou; 
- Envie um e-mail a equipe de seleção da Stefanini Campina Grande (selecao.cpg@stefanini.com) notificando a finalização do desafio para validação.

Bom desenvolvimento!
